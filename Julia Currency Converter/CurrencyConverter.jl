# Authored by Samson, Mik Iris G.

#= 
Instructions if ArgumentError on import statements: 
-> Make a Julia REPL by clicking the play button 
   on the upper right hand side on VSCode.
-> The REPL will be present on the Terminal once loaded (julia>)
   Click "]" and hit enter ("julia>" will become "(!v1.10) pkg>")
   Type "add HTTP", "add JSON"
-> Go back to Julia REPL by pressing backspace (should show: julia>)
=#

using HTTP, JSON, Printf

api_key = "apikey=fca_live_5G0EVG1qsepaMvbwyFvOCnsSklhO2qcZhdX4RLpX"
baseRequestURL = "https://api.freecurrencyapi.com/v1/latest?"
baseCurrency = "&base_currency="

# Lets a user choose their base currency 
# Step 1
function chooseBaseCurrency()
    currencies = ["PHP", "GBP", "USD", "EUR", "JPY", "THB", "KRW", "SGD", "CAD", "CNY", "AUD", "CHF", "HKD"]
  
    while true
      print("Choose your base currency from the following options:\n")

      for currency in currencies
        println("-> ", currency)
      end
  
      print("Enter your base currency: ")
      user_input = chomp(readline())
      baseChoice = uppercase(user_input)
  
      if baseChoice in currencies 
        return baseChoice  # Return the chosen base currency
      else
        println("Invalid currency. Please choose from the available options.")
      end
    end
end

# Completes the request URL
function getRequestURL(url::String, key::String, base::String)
    req = url * key * base
    return req
end

# Lets a user choose their quote currency 
# Step 2
function chooseQuoteCurrency(availableRates::Dict)

    println("\nThese are the available exchange pairs:\n")

    for (key, value) in availableRates
        println(key, " : ", value)
    end

    print("\nEnter a currency you want to convert to: ")
    currQuoteChoice = chomp(readline())
    currQuote = uppercase(currQuoteChoice)
    return currQuote

end

# Displays and allows user to pick from all available possible 
# currency to exchange to.
# Returns the exchange rate of base currency choice to intended quote currency
# Step 3
function getChoicePairRate(currQuote::String, availableRates::Dict)

    if currQuote in keys(availableRates)
        println("The currency pair " * currQuote * " was found!\n")
        pairValue = availableRates[currQuote]
        return pairValue #Float
    else
        println(currQuote * " isn't available for conversion with your currency.")
    end
    return 0
end

# The get request that provides the exchange rates for all
# possible and available currency quotes (dictionary)
function getData(url::String, key::String, base::String)

    request = getRequestURL(url, key, base)
    response = HTTP.request("GET", request) 

    if response.status == 200
        # Parse the JSON response body when response is successful
        body = String(response.body)
        currencyRates = JSON.parse(body)
        exchangeRateDict = currencyRates["data"]
        return exchangeRateDict
    else
        println("Error: Failed to retrieve currency exchange rate. Status code: ", response.status)
    end

end

# Main function for converting a chosen base currency to its equivalent
# amount in quote currency
# Step 4
function convertBaseToPair(conversionRate::Float64, currBase::String ,currQuote::String)    
    print("\nSpecify the amount in your base currency (", currBase, "): ")

    try
        baseAmountStr = chomp(readline())
        baseAmount = parse(Int, baseAmountStr)
        quoteAmount = baseAmount * conversionRate
        @printf("\n%s %d is equivalent to %s %.2f\n", currBase, baseAmount, currQuote, quoteAmount)
        println()
    catch
        println("\nError: Invalid input. Please enter a valid integer.")
    end

end


# Main run:
function main(baseCurrency)
    chosenBase = chooseBaseCurrency()
    baseCurrency = baseCurrency * chosenBase
    exchangeRates = getData(baseRequestURL, api_key, baseCurrency)
    chosenQuote = chooseQuoteCurrency(exchangeRates)
    pairRate = getChoicePairRate(chosenQuote, exchangeRates)
    convertBaseToPair(pairRate, chosenBase, chosenQuote)
end

main(baseCurrency)